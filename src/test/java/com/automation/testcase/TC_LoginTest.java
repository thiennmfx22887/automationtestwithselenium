package com.automation.testcase;
import static org.testng.Assert.assertTrue;
import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.automation.base.DriverInstance;
import com.automation.pom.LoginPage;
import com.automation.utils.CaptureScreenshot;
import com.automation.utils.PropertiesFileUtils;

public class TC_LoginTest extends DriverInstance{
	private WebDriverWait wait;
	
	String icon_SignIn = PropertiesFileUtils.getProperty("icon_signin");
	String icon_SignOut = PropertiesFileUtils.getProperty("icon_signout");
	
	
	@Test (dataProvider = "Excel")
	public void TC01_LoginFirstAccount(String email, String password) throws InterruptedException {

		driver.get(PropertiesFileUtils.getProperty("baseURL"));
		wait = new WebDriverWait(driver, 15);
		
		WebElement iconSignIn = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(icon_SignIn)));
		assertTrue(iconSignIn.isDisplayed(),"Người dùng đã đăng nhập, failed");
		iconSignIn.click();
		
		LoginPage loginPage = new LoginPage(driver);
		PageFactory.initElements(driver, loginPage);

		loginPage.enterEmail(email);
		loginPage.enterPassword(password);
		loginPage.clickSignIn();
		
		WebElement iconSignOut = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(icon_SignOut)));
		assertTrue(iconSignOut.isDisplayed(),"Icon logout chưa hiển thị");
		iconSignOut.click();
		Thread.sleep(2000);
	}
	
	@DataProvider(name = "Excel")
	public Object[][] testDataGenerator() throws Exception {
		
		FileInputStream file = new FileInputStream("./data/assignment2_data_test.xlsx");
		XSSFWorkbook workBook = new XSSFWorkbook(file);
		XSSFSheet loginSheet = workBook.getSheet("Login");
		int numberOfRowData = loginSheet.getPhysicalNumberOfRows();
		Object[][] data = new Object[numberOfRowData][2];
		
		for(int i= 0; i<numberOfRowData; i++) {
			XSSFRow row = loginSheet.getRow(i);
			XSSFCell email = row.getCell(0);
			XSSFCell passWord = row.getCell(1);
			data[i][0]= email.getStringCellValue();
			data[i][1]= passWord.getStringCellValue();
		}
		return data;
	}
    
    @AfterMethod
    public void takeScreenshot(ITestResult result){
        if (ITestResult.FAILURE == result.getStatus()) {
            try {
                String email = (String) result.getParameters()[0];
                int index = email.indexOf('@');
                String accountName = email.substring(0, index);
                CaptureScreenshot.takeScreenshot(driver, accountName);
                CaptureScreenshot.attachScreenshot(accountName);
            } catch (Exception e) {
                System.out.println("Error capturing screenshot: " + e.getMessage());
            }
        }
    }
	
}

