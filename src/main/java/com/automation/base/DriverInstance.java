package com.automation.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverInstance {
	protected static WebDriver driver;
	
	@BeforeClass
	public void init() {
//		System.setProperty("webdriver.chrome", "./drive/chromedriver.exe");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}
	
	@AfterClass
	public void closeDriverInstance() {
		driver.close();
		driver.quit();
	}

}
