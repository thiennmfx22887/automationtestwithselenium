package com.automation.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.utils.PropertiesFileUtils;

public class LoginPage {

	private WebDriverWait wait;
	
	public LoginPage(WebDriver driver) {
		wait = new WebDriverWait(driver,15);
	}
	
	public void enterEmail(String email) throws InterruptedException {
		String login_email = PropertiesFileUtils.getProperty("login_email");
		WebElement loginEmailElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(login_email)));
		loginEmailElement.clear();
		loginEmailElement.click();
		loginEmailElement.sendKeys(email);
		Thread.sleep(2000);
	}
	
	public void enterPassword(String password) throws InterruptedException {
		String login_password = PropertiesFileUtils.getProperty("login_password");
		WebElement loginPasswordElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(login_password)));
		loginPasswordElement.clear();
		loginPasswordElement.click();
		loginPasswordElement.sendKeys(password);
		Thread.sleep(2000);
	}
	
	public void clickSignIn() throws InterruptedException {
		String login_signin = PropertiesFileUtils.getProperty("login_signin");
		WebElement loginSigninElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(login_signin)));
		loginSigninElement.click();
		Thread.sleep(2000);
	}
}
