package com.automation.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesFileUtils {
	private static String CONFIG_PATH ="./configuration/configs.properties";
	
	
	public static String getProperty(String key) {
		Properties properties = new Properties();
		FileInputStream fis = null;
		String value = null;
		
		try {
			fis = new FileInputStream(CONFIG_PATH);
			properties.load(fis);
			value = properties.getProperty(key);
		}catch (Exception ex){
			System.out.println("Không lấy được property");
			ex.printStackTrace();
		}finally {
//			luôn đống luồn đọc
			if(fis!=null) {
				try {
					fis.close();
				}catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}
}
