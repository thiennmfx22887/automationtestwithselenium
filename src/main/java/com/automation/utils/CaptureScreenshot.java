package com.automation.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;
import org.testng.Reporter;

public class CaptureScreenshot {
//	static String projectPath = System.getProperty("user.dir") + "/";
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
	
	public static void takeScreenshot(WebDriver driver, String name) {
		try {
			String imageName = name + ".png";
			
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			
//			tạo thư mục nếu chưa có
			File theDir = new File("screenshots");
            if (!theDir.exists()) {
                theDir.mkdirs();
            }
            
            File destiny = new File(theDir + "/" + dateFormat.format(new Date()) + "_"  + imageName);
            FileHandler.copy(source, destiny);

		}catch (Exception ex) {
            System.out.println("Đã xảy ra lỗi khi chụp màn hình: ");
            ex.printStackTrace();
        }
	}
	
	public static void attachScreenshot(String filePath) {
		try {
			System.setProperty("org.uncommons.reportng.escape-output", "false");
			
//			lấy ra đường dẫn
			File file = new File("screenshots/" + dateFormat.format(new Date()) + "_"  + filePath + ".png");
			
			Reporter.log("<br> <a title = 'ScreenShot' href='"+ file.getAbsolutePath() + "'>"
					+ "<img alt='"+file.getName()+"' src='"+file+"' height='243' with='418'/></a></br>");

		}catch (Exception ex) {
            System.out.println("Đã xảy ra lỗi ở hàm attachScreenshot");
            ex.printStackTrace();
        }
	}
}
