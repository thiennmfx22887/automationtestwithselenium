Automation test with  selenium webdriverTestingInMobile

Apply basic knowledge of Java as well as selenium and TestNG to practice writing automation test scripts according to a given scenario.
https://automationexercise.com/

There main task are :
- Create mavenproject, configuration porn with library Selenium, TestNG, ReportNG, Guice, POI.
- Build POM architecture
- Run testcase  available data with Login function on website
- Report extraction.


Deploy on local, clone project:
With https
git clone https://gitlab.com/thiennmfx22887/automationtestwithselenium.git

With ssh
git clone git@gitlab.com:thiennmfx22887/automationtestwithselenium.git

Skills knoledge
- Java basic, oop, maven and POM architecture.
- knowsledge Selenium, TestNG, ReportNG.

Software: Eclipse IDE
